using UnityEngine.SceneManagement;
using UnityEngine;

public class TitleMenu : MonoBehaviour {
	public string levelName= "Rev2_Level1";
	    public void StartGame()
		{
        SceneManager.LoadScene(levelName);
		}

	public void ExitGame()
	{
		Application.Quit();
	}
}
