using UnityEngine;

public class Door : MonoBehaviour {
	#region Variables
	public Animation PartToAnimate;
	public bool doorOpen = false;

	private AudioSource soundS;
	public AudioClip switchSound;
	#endregion

	#region Unity Methods
	void Start () {
		soundS = gameObject.GetComponent<AudioSource>();
	}
	
	void Update () {
		
	}
	
	#endregion

	public void OpenDoor()
	{
		PartToAnimate.enabled = true;
		PartToAnimate.Play();
		PlaySound();
	}

	private void PlaySound()
	{
		soundS.clip = switchSound;
		soundS.Play();
	}
}
