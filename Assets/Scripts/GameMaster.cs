﻿using System;
using UnityEngine;

public class GameMaster : MonoBehaviour {

    public static GameMaster Instance;

    private void Awake()
    {
        if (GameMaster.Instance != null && GameMaster.Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            GameMaster.Instance = this.gameObject.GetComponent<GameMaster>();
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
