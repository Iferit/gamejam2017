﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {
    public GameObject playerPrefab;
    public Transform spawnPoint;
	// Use this for initialization
	void Start () {
        Instantiate(playerPrefab,spawnPoint.position,spawnPoint.rotation);
    }

    private void Awake()
    {
        GameObject _player = GameObject.FindGameObjectWithTag("Player");
        if (_player != null)
        {
            Destroy(this.gameObject);
        }
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
