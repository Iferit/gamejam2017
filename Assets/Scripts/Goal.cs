using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {
    public GameObject EchoPrefab;
    public string nextLevel;
    public GameObject[] goals;
    public int NumGoals;

    public bool canExit;
    public static Goal Instance;

    public AudioClip openGateSound = null;
    public AudioClip lockedGateSound = null;
    private AudioSource audioSource = null;
	// Use this for initialization
	void Start () {
        Instance = GetComponent<Goal>();
        goals = GameObject.FindGameObjectsWithTag("PreGoal");
        CheckCanExit();
        audioSource = GetComponent<AudioSource>();
		NumGoals = goals.Length;
		for (int i = 0; i < goals.Length; i++)
		{
			if (goals[i].GetComponent<FlipSwitch>().goalSwitch == false)
			{
				goals[i] = null;
				NumGoals--;
			}
		}
    }
	

    
    public void Eco()
    {
        Instantiate(EchoPrefab, transform.position, transform.rotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            CheckCanExit();
            if (canExit == true)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.clip = openGateSound;
                    audioSource.Play();
                }
                SceneManager.LoadScene(nextLevel);
            }
            else if(!audioSource.isPlaying)
            {
                audioSource.clip = lockedGateSound;
                audioSource.Play();
            }
        }
    }

    public void CheckCanExit()
    {
        canExit = true;
        foreach (GameObject flipSwitch in goals)
        {
			if (flipSwitch != null)
			{
				if (flipSwitch.GetComponent<FlipSwitch>().tripped == false)
				{
					canExit = false;
				}
			}
            
        }
    }

}
