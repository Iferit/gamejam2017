// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "PlayerMovement";
    #endregion

    #region Static
    public static Player SINGLETON = null;
    #endregion

    #region Public
    [Header("Private")]
    [SerializeField]
    private float acceleration = 10;
    [SerializeField]
    private float topSpeed = 5;
    [SerializeField]
    private float maxHealth = 10;
    [SerializeField]
    private float regenAmount = 1;
    [SerializeField]
    private float regenFrequency = 1;
    [Header("Prefabs")]
    public GameObject EcoPrefab;
    [Header("Properties")]
    public float EcoCooldown = 3f;
    private float nextEcoAvailable = 0f;
	private AudioSource playerSound;
	public AudioClip hitSound;

    [Header("Animation")]
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private float idleDanceTime = 5;

    [Header("UI")]
    [SerializeField]
    private Slider healthbar = null;
    [SerializeField]
    private GameObject goalInfoTextParent = null;
    [SerializeField]
    private Vector3 goalInfoTextOffset = Vector3.zero;
    [SerializeField]
    private Text timerText = null;
	#endregion

	#region Private
	private Rigidbody rb = null;
	

    private float currTopSpeed = 0;
    private float currMaxHealth = 0;
    private float currHealth = 0;

    private float lastInput = 0;

    private Transform goalTrans = null;
    private Transform dummyPosObj = null;
    private int textMaxSize = 0;

    private int currSecs = 0;
    private int currMins = 0;
    private float lastTime = 0;
    private bool timerPaused = false;

    private Text goalInfoText = null;

    private bool isPaused = false;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Resets the timer to zero and continues counting.
    public void ResetTimer()
    {
        currSecs = 0;
        currMins = 0;
    }
    public void ToggleTimer()
    {
        if (timerPaused) timerPaused = false;
        else timerPaused = true;
    }

    public void OnPause()
    {
        isPaused = true;
    }
    public void OnResume()
    {
        isPaused = false;
    }

	public void PlayHitSound()
	{
		playerSound.clip = hitSound;
		playerSound.Play();
	}
    #endregion

    #region Private
    // Gets the input value for both directions and adds a force to the player's rigidbody.
    private void Move()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        if (Mathf.Abs(horizontal) >= .75 && Mathf.Abs(vertical) >= .75)
        {
            horizontal /= 1.25f;
            vertical /= 1.25f;
        }
        horizontal *= acceleration;
        vertical *= acceleration;
        PrintDebugMsg("Current acceleration: " + horizontal + "/" + vertical);
        PrintDebugMsg("Current velocity: " + rb.velocity);

        if (Mathf.Abs(rb.velocity.x) > currTopSpeed) horizontal = 0;
        if (Mathf.Abs(rb.velocity.z) > currTopSpeed) vertical = 0;
        PrintDebugMsg("Current acceleration after checks: " + horizontal + "/" + vertical);

        Vector3 force = new Vector3(horizontal, 0, vertical);
        
        rb.AddForce(force);
        HandleRotation(rb.velocity);
    }
    // The central function that checks all movement and calls coresponding functions to perform movement.
    private void CheckInput()
    {
        if (!isPaused && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
        {
            Move();
            lastInput = Time.time;
        }

        if (nextEcoAvailable <= Time.time)
        {
            if (Input.GetAxis("Eco")!=0)
            { 
            Instantiate(EcoPrefab, transform.position, transform.rotation);
                nextEcoAvailable = Time.time + EcoCooldown;
                lastInput = Time.time;
            }
        }

        if (Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp("joystick button 7"))
        {
            if (Time.timeScale == 1) PauseScreen.SINGLETON.PauseGame();
            else PauseScreen.SINGLETON.ResumeGame();
        }

        if (isDebug)
        {
            if (Input.GetKeyUp(KeyCode.R)) PauseScreen.SINGLETON.ReloadLevel();

            if (Input.GetKeyUp(KeyCode.K)) Health = -1; // Deals 1 damage to player (K = Kill)
            if (Input.GetKeyUp(KeyCode.H)) Health = 1; // Heals player for 1 health (H = Heal)

            if (Input.GetKeyUp(KeyCode.U)) MaxHealth = 1;
            if (Input.GetKeyUp(KeyCode.J)) MaxHealth = -1;
        }
    }

    // Has the objects that the animator is attacked to (The graphic object) look back at the the player's last position, reset the x any z rotations to keep player standing up, and then turns them around 180 to face forward. Updates polayer's last position afterward for next pass.
    private void HandleRotation(Vector3 dir)
    {
        if(Mathf.Abs(dir.x) > .1f || Mathf.Abs(dir.z) > .1f) animator.transform.LookAt(animator.transform.position + dir);
        animator.transform.localEulerAngles = new Vector3(0, animator.transform.localEulerAngles.y, 0);
        if (isDebug) Debug.DrawLine(animator.transform.position, animator.transform.position + dir, Color.yellow);
    }

    // Actions performed when the player's health is equal to or less than 0.
    private void OnDeath()
    {
        
        PrintDebugMsg("Player died!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Updates the position of the goal's info text and updates its text.
    private void UpdateGoalInfoText()
    {
        if (dummyPosObj == null) dummyPosObj = new GameObject().transform;
        dummyPosObj.transform.position = goalTrans.position;
        dummyPosObj.transform.rotation = goalTrans.rotation;
        dummyPosObj.transform.Translate(goalInfoTextOffset, Space.Self);
        goalInfoTextParent.transform.position = Camera.main.WorldToScreenPoint(dummyPosObj.transform.position);

        string text = "";
        text += "Goal";
        Goal.Instance.CheckCanExit();
        if (Goal.Instance.canExit)
        {
            text += " unlocked!";
            goalInfoText.resizeTextMaxSize = goalInfoText.resizeTextMinSize;
        }
        else
        {
            GameObject[] goals = Goal.Instance.goals;
            int goalsFound = 0;
            foreach (GameObject goal in goals)
            {
				if (goal != null)
				{
					if (goal.GetComponent<FlipSwitch>().tripped) goalsFound++;
				}
            }
            text += " locked!\nFound " + goalsFound + "/" + Goal.Instance.NumGoals + " switches!";
            goalInfoText.resizeTextMaxSize = textMaxSize;
        }
        goalInfoText.text = text;
    }

    // Update the time and then update the UI text showing it.
    private void UpdateTimer()
    {
        if (Time.time - lastTime >= 1)
        {
            currSecs++;
            lastTime = Time.time;
        }
        if(currSecs >= 60)
        {
            currMins++;
            currSecs = 0;
        }

        timerText.text = ((currMins < 10) ? "0" : "") + currMins + ":" + ((currSecs < 10) ? "0" : "") + currSecs;
    }

    // Regenerates health if regenerating is enabled/allowed.
    private void RegenerateHealth()
    {
        if(OutOfCombat()) Health = regenAmount;
    }
    private bool OutOfCombat()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach(GameObject enemy in enemies)
        {
            if (enemy.GetComponent<Enemy>().State != EnemyState.Idle) return false;
        }

        return true;
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public float Health
    {
        get
        {
            return currHealth;
        }
        set
        {
            currHealth += value;
            if (currHealth > currMaxHealth) currHealth = currMaxHealth;
            else if (currHealth <= 0) OnDeath();

            healthbar.value = currHealth / currMaxHealth;

            PrintDebugMsg("Applying " + value + " to player's health. Health now " + currHealth + ".");
        }
    }
    public float MaxHealth
    {
        get
        {
            return currMaxHealth;
        }
        set
        {
            currMaxHealth += value;
            Health = 0;
            PrintDebugMsg("New max health is " + currMaxHealth + ".");
        }
    }
    public int[] CurrTime
    {
        get
        {
            return new int[] { currSecs, currMins };
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (Player.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one \"Player\" singletons found!");

        gameObject.tag = "Player";

        rb = GetComponent<Rigidbody>();

        currTopSpeed = topSpeed;
        currMaxHealth = maxHealth;
        currHealth = currMaxHealth;
        Health = 0;

        goalTrans = GameObject.FindGameObjectWithTag("Goal").transform;
        goalInfoText = goalInfoTextParent.transform.GetChild(0).GetComponent<Text>();
        textMaxSize = goalInfoText.resizeTextMaxSize;

        if (GameObject.FindGameObjectWithTag("PlayerSpawn") != null)
        {
            animator.transform.LookAt(GameObject.FindGameObjectWithTag("PlayerSpawn").transform);
            animator.transform.localEulerAngles = new Vector3(0, animator.transform.localEulerAngles.y, 0);
            animator.transform.Rotate(Vector3.up * 180);
        }

		playerSound = gameObject.GetComponent<AudioSource>();

        lastInput = Time.time;

        InvokeRepeating("RegenerateHealth", regenFrequency, regenFrequency);
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
		CheckInput();
	}
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        animator.SetFloat("speed", ((Mathf.Abs(rb.velocity.x) > Mathf.Abs(rb.velocity.z)) ? Mathf.Abs(rb.velocity.x) : Mathf.Abs(rb.velocity.z)));

        //CheckInput();

        if (Time.time - lastInput >= idleDanceTime) animator.SetBool("Dance", true);
        else animator.SetBool("Dance", false);

        UpdateGoalInfoText();

        if(!timerPaused) UpdateTimer();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}