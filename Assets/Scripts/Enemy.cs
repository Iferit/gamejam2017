// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

public enum EnemyState
{
    Idle,
    Pursuing,
    Investigating,
    Attacking
}

public class Enemy : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Enemy";
    #endregion

    #region Public
    [Header("Wander Settings")]
    [SerializeField]
    private float wanderMinRadius = 5;
    [SerializeField]
    private float wanderMaxRadius = 10;
    [SerializeField]
    private float wanderCooldown = 5;
    [SerializeField]
    private float waypointSearchFOV = 270;

    [Header("Pursue Settings")]
    [SerializeField]
    private float pursueRange = 10;
    [SerializeField]
    private float horizontalFOV = 180;
    [SerializeField]
    [Tooltip("Multiplies this to the speed and acceleration of the NavMeshAgent compponent on this character.")]
    private float sprintSpeedMultiplier = 2;

    [Header("Attacking Settings")]
    [SerializeField]
    private float damage = 5;
    [SerializeField]
    private float damageDelay = 1;
    [SerializeField]
    private float attackRange = 2;
    [SerializeField]
    private float attackCooldown = 2;
    [SerializeField]
    private float attackTimeLength = 1.5f;

    [Header("Audio")]
    [SerializeField]
    private AudioClip[] idleSounds = null;
    [SerializeField]
    private AudioClip[] spotSounds = null;
    [SerializeField]
    private float idleSoundDelayMin = 1;
    [SerializeField]
    private float idleSoundDelayMax = 5;
    [SerializeField]
    private bool playSpotSoundOnce = false;
    [SerializeField]
    private float spotSoundDelay = 3;
    [SerializeField]
    private bool canHearSpotAnywhere = true;
    [SerializeField]
    private GameObject footstepObj = null;
    [SerializeField]
    private float walkingPace = 2;
    [SerializeField]
    private float sprintPace = 1;
	public Playerv2 player;

    #endregion

    #region Private
    private NavMeshAgent navAgent = null;
    private float lastWander = 0;
    private Transform[] waypoints = null;
    private Transform lastWP = null;

    private float lastAttack = 0;
    private bool attackFinished = true;

    private EnemyState state = EnemyState.Idle;

    private Vector3 lastPosision;
    private Animator anim;
    private NavMeshAgent agent;

    private AudioSource audioSource = null;
    private float lastSound = 0;
    private float currSoundDelay = 0;
    private float lastStep = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public

    #endregion

    #region Private
    // Chooses a random spot in the maze within a radius and wanders to that spot.
    private void Wander()
    {
        List<Transform> points = new List<Transform>();
        foreach(Transform point in waypoints)
        {
            if (point != lastWP && CheckFOV(point, waypointSearchFOV)) points.Add(point);
        }
        PrintDebugMsg("Found " + points.Count + " valid point(s).");
        Transform chosenPoint = null;
        int randNum = 0;
        if (points.Count > 0)
        {
            randNum = Random.Range(0, points.Count - 1);
            chosenPoint = points[randNum];
        }
        else
        {
            randNum = Random.Range(0, waypoints.Length - 1);
            while (waypoints[randNum] == lastWP) randNum = Random.Range(0, waypoints.Length - 1);
            chosenPoint = waypoints[randNum];
        }

        navAgent.ResetPath();
        navAgent.SetDestination(chosenPoint.transform.position);
        lastWP = chosenPoint;

        lastWander = Time.time;
    }
    // Checks for line of sight of the player. If line of sight is reached then moves to Pursuing state.
    private void CheckForPlayer()
    {
        Vector3 lineStart = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        Vector3 lineEnd = new Vector3(player.transform.position.x, player.transform.position.y + 1.5f, player.transform.position.z);
        
        if (!Physics.Linecast(lineStart, lineEnd) && CheckFOV(player.transform, horizontalFOV))
        {
            if (state != EnemyState.Pursuing && Vector3.Distance(transform.position, player.transform.position) <= pursueRange)
            {
                state = EnemyState.Pursuing;
                navAgent.speed *= sprintSpeedMultiplier;
                navAgent.acceleration *= sprintSpeedMultiplier;
                PlaySpotSound(true);
            }
            if (isDebug) Debug.DrawLine(lineStart, lineEnd, (Vector3.Distance(transform.position, player.transform.position) <= pursueRange) ? Color.green : Color.magenta);
            PrintDebugMsg("Found Player! Pursuing...");
        }
        else if (Vector3.Distance(transform.position, player.transform.position) > pursueRange)
        {
            if(state == EnemyState.Pursuing)
            {
                state = EnemyState.Investigating;
                PrintDebugMsg("Lost player. Wandering...");
            }
            else if (state == EnemyState.Investigating && agent.pathStatus == NavMeshPathStatus.PathComplete)
            {
                state = EnemyState.Idle;
                navAgent.speed /= sprintSpeedMultiplier;
                navAgent.acceleration /= sprintSpeedMultiplier;
            }
            if (isDebug) Debug.DrawLine(lineStart, lineEnd, Color.red);
            PrintDebugMsg("Lost player. Wandering...");
        }
    }
    private bool CheckFOV(Transform obj, float fov)
    {
        bool isInFOV = false;

        Vector3 targetDir = obj.position - transform.position;
        float angleHorizontal = Vector3.Angle(targetDir, transform.forward);

        if (angleHorizontal <= fov / 2) isInFOV = true;

        PrintDebugMsg("Horizontal angle: " + Vector3.Angle(targetDir, transform.forward));
        PrintDebugMsg("CheckFOV(" + obj.gameObject.name + ") = " + isInFOV);
        return isInFOV;
    }

    // Updates the pos of the player while pursuing. Then checks to see if we should stop and return to the Idle state.
    private void UpdatePlayerPos()
    {
        navAgent.ResetPath();
        navAgent.SetDestination(player.transform.position);

        CheckForPlayer();

        if (Vector3.Distance(transform.position, player.transform.position) <= attackRange && CheckFOV(player.transform, horizontalFOV))
        {
            state = EnemyState.Attacking;
            PrintDebugMsg("Within attack range.");
        }
    }

    private void Attack()
    {
        navAgent.ResetPath();
        if (Time.time - lastAttack >= attackCooldown)
        {
            anim.Play("Attack");
            Invoke("DealDamage", damageDelay);
            attackFinished = false;
            Invoke("AttackFinished", attackTimeLength);
            lastAttack = Time.time;
        }

        if (attackFinished && (Vector3.Distance(transform.position, player.transform.position) > attackRange || !CheckFOV(player.transform, horizontalFOV)))
        {
            state = EnemyState.Pursuing;
            PrintDebugMsg("No longer within attack range. Pursuing...");
        }
    }
    private void DealDamage()
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= attackRange && CheckFOV(player.transform, horizontalFOV))
        {
            PrintDebugMsg("Dealing damage...");
			//player.PlayHitSound();
            player.Health = -damage;
        }
        else PrintDebugMsg("Missed player...");
    }
    void updateAttackAnim()
    {
        anim.SetBool("Attack", false);
    }
    private void AttackFinished()
    {
        attackFinished = true;
    }

    private void PlayIdleSound()
    {
        if (!audioSource.isPlaying && Time.time - lastSound >= currSoundDelay)
        {
            int randNum = Random.Range(0, idleSounds.Length - 1);
            audioSource.clip = idleSounds[randNum];
            if (canHearSpotAnywhere) audioSource.spatialBlend = 1;
            audioSource.Play();
            lastSound = Time.time;
            currSoundDelay = Random.Range(idleSoundDelayMin, idleSoundDelayMax);
            PrintDebugMsg("Playing " + audioSource.clip.name + " (" + randNum + ")");
        }
    }
    private void PlaySpotSound(bool force)
    {
        if ((!audioSource.isPlaying && Time.time - lastSound >= spotSoundDelay) || force)
        {
            int randNum = Random.Range(0, spotSounds.Length - 1);
            audioSource.clip = spotSounds[randNum];
            if (canHearSpotAnywhere) audioSource.spatialBlend = 0;
            audioSource.Play();
            lastSound = Time.time;
            PrintDebugMsg("Playing " + audioSource.clip.name + " (" + randNum + ")");
        }
    }
    private void PlayFootstepSound()
    {
        switch(state)
        {
            case EnemyState.Idle:
                if(Time.time - lastStep >= walkingPace)
                {
                    PrintDebugMsg("Walking Footsteps");
                    Instantiate(footstepObj, transform.position, Quaternion.identity);
                    lastStep = Time.time;
                }
                break;
            case EnemyState.Pursuing:
                if (Time.time - lastStep >= sprintPace && agent.velocity.z > 0)
                {
                    PrintDebugMsg("Sprinting Footsteps");
                    Instantiate(footstepObj, transform.position, Quaternion.identity);
                    lastStep = Time.time;
                }
                break;
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public EnemyState State
    {
        get
        {
            return state;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
		player = Playerv2.SINGLETON;
        agent = gameObject.GetComponentInChildren<NavMeshAgent>();
        anim = gameObject.GetComponentInChildren<Animator>();

        PrintDebugMsg("Loaded.");

        navAgent = GetComponent<NavMeshAgent>();

        lastWander -= wanderCooldown;
        lastAttack -= attackCooldown;

        GameObject[] waypointsFound = GameObject.FindGameObjectsWithTag("EnemyWaypoint");
        waypoints = new Transform[waypointsFound.Length];
        for(int i = 0; i < waypoints.Length; i++) waypoints[i] = waypointsFound[i].transform;
        PrintDebugMsg("Found " + waypoints.Length + " waypoint(s).");

        audioSource = GetComponent<AudioSource>();
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
		if (player == null)
		{
			player = Playerv2.SINGLETON;
		}

        anim.SetFloat("speed", Vector3.Project(agent.desiredVelocity, transform.forward).magnitude);

        switch (state)
        {
            case EnemyState.Idle:
                if(Time.time - lastWander >= wanderCooldown) Wander();
                if (isDebug) Debug.DrawLine(transform.position, navAgent.destination, Color.yellow);
                CheckForPlayer();
                PlayIdleSound();
                PlayFootstepSound();
                break;
            case EnemyState.Pursuing:
                UpdatePlayerPos();
                if(!playSpotSoundOnce) PlaySpotSound(false);
                PlayFootstepSound();
                if (isDebug)
                {
                    Debug.DrawRay(transform.position, Vector3.forward * pursueRange, Color.blue);
                    Debug.DrawRay(transform.position, Vector3.back * pursueRange, Color.blue);
                    Debug.DrawRay(transform.position, Vector3.right * pursueRange, Color.blue);
                    Debug.DrawRay(transform.position, Vector3.left * pursueRange, Color.blue);
                }
                break;
            case EnemyState.Investigating:
                CheckForPlayer();
                break;
            case EnemyState.Attacking:
                Attack();
                PlaySpotSound(false);
                break;
        }
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}