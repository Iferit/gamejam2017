﻿using UnityEngine;

public class EventTrigger : MonoBehaviour
{



	#region Variables
	[Header("Event SENDER settings: Leave NULL if on switch.")]
	public EventTrigger eventTrigger;
	public bool triggered = false;
	private bool isSender = true;
	public bool disableAfterTriggered=true;
	public float stayTriggeredTime = 10f; //only if disableAfterTriggered==false
	[Header("Event RECIVER settings")]
	public bool playAnimation; // if playing an animation, the object will not toggle enabled. 
	public GameObject objectToAnimate; //the object to animate much have its animation set to automaticly start and the animation must be disabled by default.
	private Animation TriggerAnimation; //the animation that will be triggered.
	public bool toggleEnabled; //if set, the object will be disabled or enabled. (toggles state)

	#endregion
	private void checkTriggered()
	{
		if (isSender == false)
		{
			if (eventTrigger.triggered == true)
			{
				if (TriggerAnimation.enabled == true)
				{
					TriggerAnimation.enabled = true;
				}
				return;
			}	
		}
	}

	private void ResetTriggerState()
	{
		triggered = false;
	}


	#region Unity Methods
	void Start()
	{
		if (eventTrigger == null)
		{
			isSender = true;
			triggered = false;
		}
		else
		{
			isSender = false;
		}
		if (isSender == false)
		{
			InvokeRepeating("checkTriggered", 2f, 1f);
		}

	}

	void Update()
	{
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (triggered == false)
			{
				triggered = true;
			}

		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (disableAfterTriggered == false)
			{
				Invoke("ResetTriggerState", stayTriggeredTime);
			}
		}
	}

	#endregion
}
