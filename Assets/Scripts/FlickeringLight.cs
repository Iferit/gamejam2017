﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour {
    public float minIntensity = .7f;
    public float maxIntensity = 2f;
    public float flickerRait = .3f;

    private new Light light;
    // Use this for initialization
    void Start () {
        light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        light.intensity = Mathf.Lerp(light.intensity, Random.Range(minIntensity, maxIntensity), Time.deltaTime * flickerRait);
	}
}
