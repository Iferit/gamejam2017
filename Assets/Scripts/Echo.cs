﻿using UnityEngine;

public class Echo : MonoBehaviour {

    public float expandSpeed= .3f;
    public float maxSize = 100f;
    public float sizebuffer = .6f;
    public string echoType;
    public GameObject echoSoundPrefab = null;

    private string _playerEcho = "PlayerEco";
    private string _goalEcho = "GoalEco";
    private string _goal = "Goal";
    private string _player = "Player";

    private Transform me;
    //private Material ecoShader;

    private void Start()
    {
        //ecoShader = GetComponent<MeshRenderer>().material;
        me = this.transform;
        echoType = this.gameObject.tag;
        Instantiate(echoSoundPrefab);
        //ecoShader.SetVector("CenterX", transform.position);
    }

    private void Update()
    {
        //ecoShader.SetFloat("Radius", transform.localScale.x);
        if (me.localScale.x < maxSize-sizebuffer)
        {
            float rait= me.localScale.x+(Time.deltaTime * expandSpeed);
            me.localScale = new Vector3(rait,0,rait) ;
        } else if (me.localScale.x >= maxSize - sizebuffer) 
        {
            Destroy(me.gameObject);
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        
        if (col.gameObject.tag == _player && echoType == _goalEcho)
        {
            Destroy(this.gameObject,.2f);
            return;
        }
        if (col.gameObject.tag == _goal && echoType == _playerEcho)
        {
            
            if (Goal.Instance.canExit == true)
            {
                col.gameObject.GetComponent<Goal>().Eco();
                Destroy(this.gameObject);
                return;
            }
            
        }
    }


}

