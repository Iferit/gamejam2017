using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipSwitch : MonoBehaviour {
	[Header("Triggered State")]
    public bool tripped = false;
	[Header("Switch Type")]
	public bool goalSwitch = true;
	public bool canEcho = true;
	[Header("Prefabs")]
	private Goal goal;
    public GameObject EchoPrefab;
	[SerializeField]
    private Animator leaver;
	[Header("trigger objects")]
    public FlipSwitch requiredSwitch;
	public Door door;
	private AudioSource soundS;
	public AudioClip switchSound;

    private void Start()
    {
        leaver = gameObject.GetComponent<Animator>();
		goal = Goal.Instance;

		soundS = gameObject.GetComponent<AudioSource>();
    }
    //this is a comment
    private void OnTriggerEnter(Collider other)
    {
        if (tripped == false && canEcho == true)
        {
            bool requiredFliped;
            if (requiredSwitch == null)
            {
                requiredFliped = true;
            }
            else
            {
                requiredFliped = requiredSwitch.tripped;
            }

            if (other.gameObject.tag == "PlayerEco" && requiredFliped == true)
            {
                this.Eco();
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
			if (tripped == false)
			{
				if (door != null)
				{
					door.OpenDoor();
				}
				soundS.clip = switchSound;
				soundS.Play();
				tripped = true;
				leaver.SetBool("Tripped", true);
			}
			
        }
    }

    public void Eco()
    {
        Instantiate(EchoPrefab, transform.position, transform.rotation);
    }

    private void OnDrawGizmos()
    {
        if (requiredSwitch != null)
        {
            Vector3 midPoint = new Vector3();
            // midPoint = requiredSwitch.transform.position - (transform.position/2);
            midPoint = (transform.position/2) + (requiredSwitch.transform.position/2 );
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, midPoint);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(midPoint, requiredSwitch.transform.position);
        }

		if (door != null)
		{
			Vector3 midPoint = new Vector3();
			// midPoint = requiredSwitch.transform.position - (transform.position/2);
			midPoint = (transform.position / 2) + (door.transform.position / 2);
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, midPoint);
			Gizmos.color = Color.green;
			Gizmos.DrawLine(midPoint, door.transform.position);
		}
	}
}
