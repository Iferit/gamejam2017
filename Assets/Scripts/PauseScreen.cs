// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseScreen : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebugPauseScreen = false;
    private string debugScriptNamePauseScreen = "PauseScreen";
    #endregion

    #region Static
    public static PauseScreen SINGLETON = null;
    #endregion

    #region Public
    [SerializeField]
    private GameObject pausePanel = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Pause/resume functions simply change timescale to 0 when paused (freezes evreything like physics) and then returns it to 1 when resumed, real-time.
    public void PauseGame()
    {
        PrintDebugMsg("Pausing game...");
        pausePanel.SetActive(true);
        pausePanel.transform.GetChild(1).GetComponent<Button>().Select();
        Playerv2.SINGLETON.OnPause();
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        PrintDebugMsg("Resuming game...");
        pausePanel.SetActive(false);
		Playerv2.SINGLETON.OnResume();
        Time.timeScale = 1;
    }
    // Reloads the current level.
    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    // Load the main menu.
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }
    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebugPauseScreen) Debug.Log(debugScriptNamePauseScreen + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptNamePauseScreen + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptNamePauseScreen + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters_Properties
    
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");

        if (PauseScreen.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("PauseScreen singleton already exists!");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {

    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}