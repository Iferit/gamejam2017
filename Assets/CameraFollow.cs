using UnityEngine;

public class CameraFollow : MonoBehaviour {
	#region Variables
	public Transform target;
	public Vector3 offset;
	public float smoothSpeed=0.125f;

	#endregion
	
	#region Unity Methods
	void FixedUpdate () {
		Vector3 desiredPosition = target.position + offset;
		Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
		transform.position = smoothedPosition;

		transform.LookAt(target);
	}
	
	#endregion


}
